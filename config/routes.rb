Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :heroes
      # get 'heroes/find' => 'heroes#find_hero', as: :find_hero
    end
  end
end
