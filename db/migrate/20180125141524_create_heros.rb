class CreateHeros < ActiveRecord::Migration[5.1]
  def change
    create_table :heros, id: :uuid do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
