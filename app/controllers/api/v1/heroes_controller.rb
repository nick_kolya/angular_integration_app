class Api::V1::HeroesController < Api::V1::BaseController
  def index
    respond_with Hero.ransack(name_cont: params[:name])&.result
  end

  def show
    respond_with Hero.find(params[:id])
  end

  def create
    respond_with :api, :v1, Hero.create(hero_params)
  end

  def destroy
    respond_with :api, :v1, Hero.destroy(params[:id])
  end

  def update
    hero = Hero.find(params[:id])
    hero.update_attributes(hero_params)

    render json: hero
  end

  private

  def hero_params
    params.require(:hero).permit(:name)
  end
end
