FactoryGirl.define do
  factory :hero do
    sequence(:name) { |n| "hero#{n}" }
  end
end
