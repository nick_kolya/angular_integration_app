require 'spec_helper'

describe Api::V1::HeroesController do
  before(:each) { 4.times { create :hero } }

  describe '#index' do
    subject { get :index, format: :json }

    before { subject }

    it 'returns correct data' do
      expect(JSON.parse(response.body).count).to eq Hero.count
    end
  end
end
